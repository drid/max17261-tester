/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
struct power_status {
	uint16_t voltage; 			/**< Voltage in mV */
	uint16_t voltage_alt; 		/**< Voltage in mV sensed from STM32*/
	uint16_t voltage_avg; 		/**< Average Voltage in mV */
	uint16_t voltage_max; 		/**< Max Voltage in mV */
	uint16_t voltage_min; 		/**< Min Voltage in mV */
	int16_t current; 			/**< Current in mA */
	int16_t current_avg; 		/**< Average Current in mA */
	int16_t current_max; 		/**< Max in mA */
	int16_t current_min; 		/**< Min in mA */
	int8_t temperature_die; 	/**< Current in mA */
	int8_t temperature; 		/**< Current in mA */
	int8_t temperature_avg; 	/**< Average Current in mA */
	int8_t temperature_max; 	/**< Max in mA */
	int8_t temperature_min; 	/**< Min in mA */
	int8_t SOC; 				/**< State of charge in % */
	int16_t FullRepCap;			/**< new full-capacity value  */
	int16_t RepCap;				/**< reported remaining capacity in mAh. */
	uint16_t TTE; 				/**< Time to empty in minutes */
};

struct app_params {
	uint8_t display_mode;
	uint8_t refresh_interval;

};
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
typedef enum {
	DM_TABLE,
	DM_SINGLE_LINE_HR,
	DM_ROLLING_HR,
	DM_CSV,
	DM_TSV
} DISPLAY_MODE_TypDef;
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
