/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <max17261_driver.h>
#include <string.h>
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
struct max17261_conf hmax17261;
struct power_status power_status;
uint8_t ret;
char textbuff[500];
char cmd = 0;
struct app_params app_params;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SD_HandleTypeDef hsd1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_SDMMC1_SD_Init(void);
/* USER CODE BEGIN PFP */
void uart_printf(char*);
HAL_StatusTypeDef update_power_status(struct power_status*);
void display_data(struct power_status ps, struct app_params params);
void display_model_params(struct power_status*);
void dump_model_(struct power_status*);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */
	app_params.display_mode = DM_TABLE;
	/* USER CODE END 1 */
	app_params.refresh_interval = 1;

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_I2C1_Init();
	MX_SDMMC1_SD_Init();
	MX_FATFS_Init();
	/* USER CODE BEGIN 2 */
	/* Setup MAX17261 */
	hmax17261.DesignCap = BATTERY_CAPACITY;
	hmax17261.IchgTerm = BATTERY_CRG_TERM_I;
	hmax17261.VEmpty = (BATTERY_V_EMPTY << 7) | (BATTERY_V_Recovery & 0x7F);
	hmax17261.R100 = 1;
	hmax17261.ChargeVoltage = POWER_CHG_VOLTAGE;
//	hmax17261.ExtT = 1;
//	hmax17261.lparams.FullCapNom = BATTERY_CAPACITY;
//	hmax17261.lparams.FullCapRep = BATTERY_CAPACITY;
//	hmax17261.lparams.RCOMP0 = 0x0070;
//	hmax17261.lparams.TempCo = 0x223E;
//	hmax17261.lparams.cycles = 0;
	hmax17261.force_init = 0;
	uart_printf("\n\n\rMAX17261 Tester\n\n\r");
	sprintf(textbuff, "Initialising MAX17261 at 0x%X\n\r", MAX17261_ADDRESS);
	uart_printf(textbuff);
	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, SET);
	ret = max17261_init(&hmax17261);
	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, RESET);

	if (ret != 0) {
		uart_printf("Failed to initialise device\n\r\n");
	}
	uart_printf("Commands: t: table, s: single line, c: CSV T:TSV\n\r\n");
	HAL_UART_Receive_IT(&huart2, (uint8_t*) &cmd, 1);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
		if (ret == 1)
			continue;
		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, SET);
		update_power_status(&power_status);
		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, RESET);

		switch (cmd) {
		case 's':
			app_params.display_mode = DM_SINGLE_LINE_HR;
			uart_printf("\n\n\n\n\n\n\n\n\r");
			break;
		case 'c':
			app_params.display_mode = DM_CSV;
			uart_printf("\n\n\n\n\n\n\n\n\r");
			uart_printf("Time, SOC,V,V Min, V Max, V Avg,C,C Min,C Max,C Avg,T,T Min,T Max,T Avg,T Die, RepCap\r\n");
			break;
		case 't':
			app_params.display_mode = DM_TABLE;
			uart_printf("\n\n\n\n\n\n\n\n\r");
			break;
		case 'T':
			app_params.display_mode = DM_TSV;
			uart_printf("\n\n\n\n\n\n\n\n\r");
			break;
		default:
			break;
		}
		cmd = 0;
		display_data(power_status, app_params);
		HAL_Delay(app_params.refresh_interval*1000);
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1)
			!= HAL_OK) {
		Error_Handler();
	}
	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 10;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void) {

	/* USER CODE BEGIN I2C1_Init 0 */

	/* USER CODE END I2C1_Init 0 */

	/* USER CODE BEGIN I2C1_Init 1 */

	/* USER CODE END I2C1_Init 1 */
	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing = 0x10909CEC;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c1) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Analogue filter
	 */
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE)
			!= HAL_OK) {
		Error_Handler();
	}
	/** Configure Digital filter
	 */
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN I2C1_Init 2 */

	/* USER CODE END I2C1_Init 2 */

}

/**
 * @brief SDMMC1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SDMMC1_SD_Init(void) {

	/* USER CODE BEGIN SDMMC1_Init 0 */

	/* USER CODE END SDMMC1_Init 0 */

	/* USER CODE BEGIN SDMMC1_Init 1 */

	/* USER CODE END SDMMC1_Init 1 */
	hsd1.Instance = SDMMC1;
	hsd1.Init.ClockEdge = SDMMC_CLOCK_EDGE_RISING;
	hsd1.Init.ClockBypass = SDMMC_CLOCK_BYPASS_DISABLE;
	hsd1.Init.ClockPowerSave = SDMMC_CLOCK_POWER_SAVE_DISABLE;
	hsd1.Init.BusWide = SDMMC_BUS_WIDE_1B;
	hsd1.Init.HardwareFlowControl = SDMMC_HARDWARE_FLOW_CONTROL_DISABLE;
	hsd1.Init.ClockDiv = 0;
	/* USER CODE BEGIN SDMMC1_Init 2 */

	/* USER CODE END SDMMC1_Init 2 */

}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void) {

	/* USER CODE BEGIN USART2_Init 0 */

	/* USER CODE END USART2_Init 0 */

	/* USER CODE BEGIN USART2_Init 1 */

	/* USER CODE END USART2_Init 1 */
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart2) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART2_Init 2 */

	/* USER CODE END USART2_Init 2 */

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : LD2_Pin */
	GPIO_InitStruct.Pin = LD2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PC11 */
	GPIO_InitStruct.Pin = GPIO_PIN_11;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void uart_printf(char *msg) {
	HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), 1000);
}

uint8_t update_power_status(struct power_status *power_status) {
	HAL_StatusTypeDef ret = HAL_OK;
	power_status->voltage = max17261_get_voltage(&hmax17261);
	power_status->voltage_avg = max17261_get_average_voltage(&hmax17261);
	power_status->current = max17261_get_current(&hmax17261);
	power_status->current_avg = max17261_get_average_current(&hmax17261);
	power_status->SOC = max17261_get_SOC(&hmax17261);
	power_status->RepCap = max17261_get_reported_capacity(&hmax17261);
	power_status->FullRepCap = max17261_get_full_reported_capacity(&hmax17261);
	power_status->temperature = max17261_get_temperature(&hmax17261);
	power_status->temperature_die = max17261_get_die_temperature(&hmax17261);
	power_status->temperature_avg = max17261_get_average_temperature(
			&hmax17261);
	power_status->TTE = max17261_get_TTE(&hmax17261);

	max17261_get_minmax_voltage(&hmax17261, &power_status->voltage_min,
			&power_status->voltage_max);
	max17261_get_minmax_current(&hmax17261, &power_status->current_min,
			&power_status->current_max);
	max17261_get_minmax_temperature(&hmax17261, &power_status->temperature_min,
			&power_status->temperature_max);
	max17261_get_learned_params(&hmax17261);
	ret |= max17261_get_qrtable_values(&hmax17261);
	return ret;
}

void display_data(struct power_status ps, struct app_params params) {
	uint32_t systick;
	systick = HAL_GetTick() / 1000;
	switch (params.display_mode) {
	case DM_SINGLE_LINE_HR:
		snprintf(textbuff, sizeof(textbuff),
				"%ld|SOC:%02d%% V:%04d VStat:%04d/%04d/%04d C:%04d CStat:%04d/%04d/%04d T:%03d|%03d/%03d/%03d TD:%03d Cap:%04d/%04d\r",
				systick, power_status.SOC, power_status.voltage,
				power_status.voltage_min, power_status.voltage_max,
				power_status.voltage_avg, power_status.current,
				power_status.current_min, power_status.current_max,
				power_status.current_avg, power_status.temperature,
				power_status.temperature_min, power_status.temperature_max,
				power_status.temperature_avg, power_status.temperature_die,
				power_status.RepCap, power_status.FullRepCap);

		break;
	case DM_TABLE:
		snprintf(textbuff, sizeof(textbuff),
				"Time: %06ld\tSOC:%02d%%\tCapacity: %d/%d\r\n"
				"\tInst\tMin\tMax\tAverage\r\n"
				"Voltage\t%04d\t%04d\t%04d\t%04d\r\n"
				"Current\t%04d\t%04d\t%04d\t%04d\r\n"
				"Temp\t%d°C\t%d°C\t%d°C\t%d°C\r\n"
				"Die T\t%d°C\r\n"
				"FullRepCap:%d FullCapNom:%d \tRCOMP:%d\tTempCo:%d\tCycles:%d\r\n"
				"QRTables: \t%X \t%X \t%X \t%X\n\r"
				"\e[A\e[A\e[A\e[A\e[A\e[A\e[A\e[A",
				systick, power_status.SOC,
				power_status.RepCap, power_status.FullRepCap,
				power_status.voltage, power_status.voltage_min,
				power_status.voltage_max, power_status.voltage_avg,
				power_status.current, power_status.current_min,
				power_status.current_max, power_status.current_avg,
				power_status.temperature, power_status.temperature_min,
				power_status.temperature_max, power_status.temperature_avg,
				power_status.temperature_die, hmax17261.lparams.FullCapRep,
				hmax17261.lparams.FullCapNom, hmax17261.lparams.RCOMP0,
				hmax17261.lparams.TempCo, hmax17261.lparams.cycles,
				hmax17261.lparams.QRTable[0],hmax17261.lparams.QRTable[1],
				hmax17261.lparams.QRTable[2],hmax17261.lparams.QRTable[3]);

		break;
	case DM_CSV:
		snprintf(textbuff, sizeof(textbuff),
				"%ld,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n", systick,
				power_status.SOC, power_status.voltage,
				power_status.voltage_min, power_status.voltage_max,
				power_status.voltage_avg, power_status.current,
				power_status.current_min, power_status.current_max,
				power_status.current_avg, power_status.temperature,
				power_status.temperature_min, power_status.temperature_max,
				power_status.temperature_avg, power_status.temperature_die,
				power_status.RepCap);
		break;
	case DM_TSV:
		snprintf(textbuff, sizeof(textbuff),
				"%ld\t%f\t%f\t%d\t%f\t%f\t%f\t%f\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%d\r\n", systick,
				(double)power_status.voltage/1000, (double)power_status.current/1000, power_status.SOC,
				(double)power_status.voltage_min/1000, (double)power_status.voltage_max/1000,
				(double)power_status.voltage_avg/1000, (double)power_status.current_min/1000,
				(double)power_status.current_max/1000, (double)power_status.current_avg/1000,
				power_status.temperature, power_status.temperature_min,
				power_status.temperature_max, power_status.temperature_avg,
				power_status.temperature_die, power_status.RepCap);
	default:
		break;
	}
	uart_printf(textbuff);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	HAL_UART_Receive_IT(huart, (uint8_t*) &cmd, 1);
}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
