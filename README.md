# MAX17261 Tester

Simple MAX17261 Testing firmware for NUCLEO-L476RG  

Serial output of basic MAX17261 parameters

## Usage

Connect SCL to PB6 and SDA to PB7  
Open a serial terminal at 115200,8,N,1  

A connection to MAX17261 will be attempted and if successful, parameter values will be displayed every 1000ms

If connection fails, reset NUCLEO to retry
